# Contributor: Artem Korezin <source-email@yandex.ru>
# Maintainer: Artem Korezin <source-email@yandex.ru>
pkgname=py3-pykerberos
_pkgname=pykerberos
# renovate: datasource=pypi depName=pykerberos
pkgver=1.2.4
pkgrel=99
pkgdesc='High-level interface to Kerberos'
url='https://github.com/02strich/pykerberos'
arch='x86_64'
license='Apache-2.0'

# renovate: datasource=repology depName=alpine_3_21/python3 versioning=loose
depends="$depends python3=3.12.9-r0"
# renovate: datasource=repology depName=alpine_3_21/krb5 versioning=loose
depends="$depends krb5=1.21.3-r0"

# renovate: datasource=repology depName=alpine_3_21/krb5 versioning=loose
makedepends="$makedepends krb5-dev=1.21.3-r0"
# renovate: datasource=repology depName=alpine_3_21/python3 versioning=loose
makedepends="$makedepends python3-dev=3.12.9-r0"
# renovate: datasource=repology depName=alpine_3_21/py3-gpep517 versioning=loose
makedepends="$makedepends py3-gpep517=16-r0"
# renovate: datasource=repology depName=alpine_3_21/py3-setuptools versioning=loose
makedepends="$makedepends py3-setuptools=70.3.0-r0"
# renovate: datasource=repology depName=alpine_3_21/py3-wheel versioning=loose
makedepends="$makedepends py3-wheel=0.43.0-r0"

source="https://files.pythonhosted.org/packages/source/${_pkgname::1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
options='!check'
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

package() {
	python3 -m installer \
		--destdir="$pkgdir" \
		dist/*.whl
}
sha512sums="
92adb2c788fb5be78d2a7f65b9292534cea0c122f4cd27868d672ffd25b40d11d950d13d4b8636d8d5d6816b045ca8e13a649ce909a99361e494fb779eca79d8  pykerberos-1.2.4.tar.gz
"
