#!/usr/bin/env sh

set -e

eval "$(ssh-agent -s)"

exec "$@"
