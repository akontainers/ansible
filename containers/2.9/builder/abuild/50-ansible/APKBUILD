# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Takuya Noguchi <takninnovationresearch@gmail.com>
# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Contributor: Artem Korezin <source-email@yandex.ru>
# Maintainer: Artem Korezin <source-email@yandex.ru>
pkgname=ansible
_pkgname=ansible
# renovate: datasource=pypi depName=ansible
pkgver=2.9.27
pkgrel=99
pkgdesc='A configuration-management, deployment, task-execution, and multinode orchestration framework'
url='https://ansible.com/'
arch='noarch'
license='GPL-3.0-or-later'

# renovate: datasource=repology depName=alpine_3_17/python3 versioning=loose
depends="$depends python3=3.10.15-r0"
# renovate: datasource=repology depName=alpine_3_17/py3-yaml versioning=loose
depends="$depends py3-yaml=6.0-r0"
# renovate: datasource=repology depName=alpine_3_17/py3-jinja2 versioning=loose
depends="$depends py3-jinja2=3.1.4-r0"
# renovate: datasource=repology depName=alpine_3_17/py3-cryptography versioning=loose
depends="$depends py3-cryptography=38.0.3-r1"
# renovate: datasource=repology depName=alpine_3_17/openssh versioning=loose
depends="$depends openssh-client=9.1_p1-r6"

# renovate: datasource=repology depName=alpine_3_17/py3-setuptools versioning=loose
makedepends="$makedepends py3-setuptools=70.3.0-r0"

source="https://files.pythonhosted.org/packages/source/${_pkgname::1}/$_pkgname/$_pkgname-$pkgver.tar.gz
		ufw_add_vrrp.patch
		0001_ansible_galaxy_progress.patch"
options='!check'

build() {
	python3 -B setup.py build
}

package() {
	python3 -B setup.py install --prefix=/usr --root="$pkgdir"
}
sha512sums="
99987b8a1d243ef3496d66178774c33b05951daaff584b12e645c0176391805f90d00780e86adec01316b28645287489326218c0de7c10084552da90848735c7  ansible-2.9.27.tar.gz
564dbc2488dcf8931f31963b9f6e0643e14ac3d36273ed2ecf3acd78a03b70e3d343dc68ea6e8e30eaf7870d0f1bbebb46db237a07250a00fd30cd593310e483  ufw_add_vrrp.patch
fa4980e97bcdc8f3ae3af3ba40e592e127b76c9a39de3252fbb59601a2758a8a9b7ba6795a99be84ca5b4ebf271eff30c19100244347f9f987f0e10e5ea17ea9  0001_ansible_galaxy_progress.patch
"
