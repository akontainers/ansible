FROM registry.gitlab.com/akontainers/apkbuild-tools:0.1.28@sha256:22635b7ee4c4c051c0abc661faadaa7a94c90f92195ef3bfed742c93ecc39248 AS apkbuild-tools

FROM registry.gitlab.com/akontainers/alpine:3.17.10@sha256:e7221b135ce322d79babc42f62c96cb64f0d9f07838a247fca85591e68f6b18e AS builder

COPY --from=apkbuild-tools /usr/sbin/builder /usr/sbin/builder
RUN --mount=type=cache,target=/etc/apk/cache,sharing=locked,id=alpine_3.17_apk_cache \
    builder prepare
COPY --chown=abuild:abuild builder/abuild /home/abuild
USER abuild
RUN --mount=type=cache,target=/etc/apk/cache,sharing=locked,id=alpine_3.17_apk_cache \
    builder build

FROM registry.gitlab.com/akontainers/alpine:3.17.10@sha256:e7221b135ce322d79babc42f62c96cb64f0d9f07838a247fca85591e68f6b18e

# renovate: datasource=repology depName=alpine_3_17/git versioning=loose
ARG GIT_VERSION=2.39.5-r0
# renovate: datasource=repology depName=alpine_3_17/py3-openssl versioning=loose
ARG PY3_OPENSSL_VERSION=22.1.0-r1
# renovate: datasource=repology depName=alpine_3_17/py3-dnspython versioning=loose
ARG PY3_DNSPYTHON_VERSION=2.2.1-r0
# renovate: datasource=repology depName=alpine_3_17/py3-netaddr versioning=loose
ARG PY3_NETADDR_VERSION=0.8.0-r3
# renovate: datasource=repology depName=alpine_3_17/py3-zabbix versioning=loose
ARG PY3_ZABBIX_VERSION=0.5.4-r4
# renovate: datasource=pypi depName=ansible
ARG ANSIBLE_VERSION=2.9.27

# hadolint ignore=DL3018,DL3019
RUN --mount=type=cache,target=/etc/apk/cache,sharing=locked,id=alpine_3.17_apk_cache \
    --mount=type=cache,target=/packages,source=/home/abuild/packages/abuild,from=builder \
    --mount=type=bind,target=/etc/apk/keys,source=/etc/apk/keys,from=builder \
    set -eux; \
      echo '/packages' >> /etc/apk/repositories; \
      apk add \
        "git=${GIT_VERSION}" \
        "py3-openssl=${PY3_OPENSSL_VERSION}" \
        "py3-dnspython=${PY3_DNSPYTHON_VERSION}" \
        "py3-netaddr=${PY3_NETADDR_VERSION}" \
        "py3-zabbix=${PY3_ZABBIX_VERSION}" \
        "ansible=${ANSIBLE_VERSION}-r99" \
      ; \
      find /usr -type d -name __pycache__ -prune -exec rm -rf {} \;; \
      sed '/^\/packages$/d' -i /etc/apk/repositories; \
      mkdir -m 0700 ~/.ssh;

COPY rootfs /

ENTRYPOINT [ "entrypoint.sh" ]
CMD [ "sh" ]
