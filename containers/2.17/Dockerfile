FROM registry.gitlab.com/akontainers/apkbuild-tools:0.1.28@sha256:22635b7ee4c4c051c0abc661faadaa7a94c90f92195ef3bfed742c93ecc39248 AS apkbuild-tools

FROM registry.gitlab.com/akontainers/alpine:3.21.3@sha256:a1e82f89856116414758d5b06abcd083adb72a764fa9802b1b8ece72cb893122 AS builder

COPY --from=apkbuild-tools /usr/sbin/builder /usr/sbin/builder
RUN --mount=type=cache,target=/etc/apk/cache,sharing=locked,id=alpine_3.21_apk_cache \
    builder prepare
COPY --chown=abuild:abuild builder/abuild /home/abuild
USER abuild
RUN --mount=type=cache,target=/etc/apk/cache,sharing=locked,id=alpine_3.21_apk_cache \
    builder build

FROM registry.gitlab.com/akontainers/alpine:3.21.3@sha256:a1e82f89856116414758d5b06abcd083adb72a764fa9802b1b8ece72cb893122

# renovate: datasource=repology depName=alpine_3_21/git versioning=loose
ARG GIT_VERSION=2.47.2-r0
# renovate: datasource=repology depName=alpine_3_21/py3-openssl versioning=loose
ARG PY3_OPENSSL_VERSION=24.1.0-r1
# renovate: datasource=repology depName=alpine_3_21/py3-dnspython versioning=loose
ARG PY3_DNSPYTHON_VERSION=2.7.0-r0
# renovate: datasource=repology depName=alpine_3_21/py3-netaddr versioning=loose
ARG PY3_NETADDR_VERSION=0.10.1-r1
# renovate: datasource=repology depName=alpine_3_21/py3-zabbix versioning=loose
ARG PY3_ZABBIX_VERSION=0.5.6-r1
# renovate: datasource=pypi depName=pywinrm
ARG PY3_PYWINRM_VERSION=0.5.0
# renovate: datasource=pypi depName=pykerberos
ARG PY3_PYKERBEROS_VERSION=1.2.4
# renovate: datasource=pypi depName=ansible-core
ARG ANSIBLE_CORE_VERSION=2.17.9

# hadolint ignore=DL3018,DL3019
RUN --mount=type=cache,target=/etc/apk/cache,sharing=locked,id=alpine_3.21_apk_cache \
    --mount=type=cache,target=/packages,source=/home/abuild/packages/abuild,from=builder \
    --mount=type=bind,target=/etc/apk/keys,source=/etc/apk/keys,from=builder \
    set -eux; \
      echo '/packages' >> /etc/apk/repositories; \
      apk add !pyc \
        "git=${GIT_VERSION}" \
        "py3-openssl=${PY3_OPENSSL_VERSION}" \
        "py3-dnspython=${PY3_DNSPYTHON_VERSION}" \
        "py3-netaddr=${PY3_NETADDR_VERSION}" \
        "py3-zabbix=${PY3_ZABBIX_VERSION}" \
        "py3-pywinrm=${PY3_PYWINRM_VERSION}-r99" \
        "py3-pykerberos=${PY3_PYKERBEROS_VERSION}-r99" \
        "ansible-core=${ANSIBLE_CORE_VERSION}-r99" \
      ; \
      sed '/^\/packages$/d' -i /etc/apk/repositories; \
      mkdir -m 0700 ~/.ssh;

COPY rootfs /

ENTRYPOINT [ "entrypoint.sh" ]
CMD [ "sh" ]
