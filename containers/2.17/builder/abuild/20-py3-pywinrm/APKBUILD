# Contributor: Artem Korezin <source-email@yandex.ru>
# Maintainer: Artem Korezin <source-email@yandex.ru>
pkgname=py3-pywinrm
_pkgname=pywinrm
# renovate: datasource=pypi depName=pywinrm
pkgver=0.5.0
pkgrel=99
pkgdesc='Python library for Windows Remote Management'
url='http://github.com/diyan/pywinrm/'
arch='noarch'
license='MIT'

# renovate: datasource=repology depName=alpine_3_21/python3 versioning=loose
depends="$depends python3=3.12.9-r0"
# renovate: datasource=repology depName=alpine_3_21/py3-xmltodict versioning=loose
depends="$depends py3-xmltodict=0.13.0-r4"
# renovate: datasource=repology depName=alpine_3_21/py3-requests versioning=loose
depends="$depends py3-requests=2.32.3-r0"
# renovate: datasource=repology depName=alpine_3_21/py3-six versioning=loose
depends="$depends py3-six=1.16.0-r9"

# renovate: datasource=repology depName=alpine_3_21/py3-gpep517 versioning=loose
makedepends="$makedepends py3-gpep517=16-r0"
# renovate: datasource=repology depName=alpine_3_21/py3-setuptools versioning=loose
makedepends="$makedepends py3-setuptools=70.3.0-r0"
# renovate: datasource=repology depName=alpine_3_21/py3-wheel versioning=loose
makedepends="$makedepends py3-wheel=0.43.0-r0"

subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/${_pkgname::1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
options='!check'
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

package() {
	python3 -m installer \
		--destdir="$pkgdir" \
		dist/*.whl
}
sha512sums="
9a5a3ebe5b2ec3daf4417e0b43143b415d652ebd0f78bd04cfbe6d69aac1c45d012742f0ec754156131869c4695f94e993edefd1e8d4c4d88245bf628a6cdda5  pywinrm-0.5.0.tar.gz
"
